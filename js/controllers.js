angular.module('starter.controllers', [])

    .controller('NavigationCtrl', function ($scope, $location) {
        $scope.isActive = function (viewLocation) {
            var active = (viewLocation === $location.path());
            return active;
        };
    })

    .controller('ConversionCtrl', ($scope, $state) => {

        $scope.title = $state.current.data.title;
        $scope.units = $state.current.data.units.map(u => ({ id: u, name: Measure.units[u].name }));
        $scope.input = '';
        $scope.inputUnit = $scope.units[0].id;
        $scope.outputUnit = $scope.inputUnit;

        $scope.output = function () {
            if ($scope.input) {
                var value = new Measure($scope.input + $scope.inputUnit);
                return value.to($scope.outputUnit).toFixed(3);
            } else {
                return 0;
            }
        }
    })

    .controller('DryGasRateCtrl', function ($scope) {
        $scope.chokeSize = '';
        $scope.wellHeadPressureUpstream = 0;
        $scope.wellHeadPressureUnitUpstream = 'psi';
        $scope.wellHeadPressureDownstream = 0;
        $scope.wellHeadPressureUnitDownstream = 'psi';
        $scope.wellHeadTemperature = 0;
        $scope.wellHeadTemperatureUnit = 'F';
        $scope.gasGravity = 1;
        $scope.outputUnit = 'Mcf/d';

        $scope.output = function () {
            if ($scope.chokeSize && $scope.wellHeadPressureUpstream && $scope.wellHeadPressureDownstream &&
                $scope.wellHeadTemperature && $scope.gasGravity) {

                var x = $scope.chokeSize / 64;
                var c = 493.8 * x * x - 24.6 * x + 1.79;
                var whp = new Measure($scope.wellHeadPressureUpstream + $scope.wellHeadPressureUnitUpstream).to('psi').value -
                    new Measure($scope.wellHeadPressureDownstream + $scope.wellHeadPressureUnitDownstream).to('psi').value;

                var wht = new Measure($scope.wellHeadTemperature + $scope.wellHeadTemperatureUnit).to('F').value;
                var g = $scope.gasGravity;

                var value = c * whp / Math.sqrt(g * (wht + 460));
                if (isNaN(value) || !isFinite(value) || value <= 0) {
                    return ':(';
                }

                return new Measure(value + 'Mcf/d').to($scope.outputUnit).toFixed(3);
            } else {
                return 0;
            }
        }
    })

    .controller('PlateChoiceCtrl', function ($scope) {

        var hx = 198, ax = 723, d2x = 986, d4x = 1060, d5x = 1134;
        var d2p = [5102.247, 0.1161762, 331913600000, -96174.46], pd2 = [2.157545, 2.416431, 654.0206, -0.1134879];
        var d4p = [7701.467, 0.09433247, 617141300000, -86614.97], pd4 = [3.588486, 1.813842, 648.8261, -0.3295979];
        var d5p = [8607.565, 0.1007326, 694507000000, -113370.2], pd5 = [4.753775, 1.563039, 735.2207, -0.6296209];
        var hp = [35016.22, 0.01530214, 0.000001252066, -25157.42];

        function fit(x, coeff) {
            return coeff[3] + (coeff[0] - coeff[3]) / (1 + Math.pow(x / coeff[2], coeff[1]));
        }

        $scope.diameter = '2';
        $scope.initialPressure = '';
        $scope.initialOrifice = 0;
        $scope.finalPressure = 0;

        $scope.output = function () {
            if (!$scope.initialOrifice || !$scope.initialPressure || !$scope.finalPressure) {
                return 0;
            }

            var dx = d2x;
            var dp = d2p;
            var pd = pd2;

            if ($scope.diameter == '4') {
                dx = d4x;
                dp = d4p;
                pd = pd4;
            } else if ($scope.diameter == '5') {
                dx = d5x;
                dp = d5p;
                pd = pd5;
            }

            var c = (ax - hx) / (dx - hx);
            var dy = fit($scope.initialOrifice, dp);
            var hy = fit($scope.initialPressure, hp);
            var ay = (dy - hy) * c + hy;
            var h1y = fit($scope.finalPressure, hp);
            var d1y = (ay - h1y) / c + h1y;

            var result = fit(d1y, pd);
            if (isNaN(result) || !isFinite(result) || result <= 0 || result > 100) {
                return ':(';
            }

            return result.toFixed(3);
        }
    })
;