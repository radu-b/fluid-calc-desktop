
angular.module('starter', ['ui.router', 'starter.controllers'])

    .directive('autofocus', ['$timeout',
        function ($timeout) {
            return {
                restrict: 'A',
                link: function ($scope, $element) {
                    $timeout(function () {
                        $element[0].focus();
                        if ($element[0].tagName == 'INPUT') {
                            $element[0].select();
                        }
                    });
                }
            };
        }
    ])

    .config(function ($stateProvider, $urlRouterProvider) {
        $stateProvider

            .state('conversion-temperature', {
                url: '/conversion-temperature',
                templateUrl: 'templates/conversion.html',
                controller: 'ConversionCtrl',
                data: {
                    title: 'Temperature',
                    units: ['C', 'F', 'K', 'R']
                }
            })

            .state('conversion-pressure', {
                url: '/conversion-pressure',
                templateUrl: 'templates/conversion.html',
                controller: 'ConversionCtrl',
                data: {
                    title: 'Pressure',
                    units: ['psi', 'bar', 'Pa', 'atm', 'at', 'inH2O', 'mbar', 'dyn/cm2', 'mmH2O', 'cmH2O4', 'cmH2O', 'inH2O39', 'inH2O60',
                        'ftH2O39', 'ftH2O', 'gf/cm2', 'mmHg', 'cmHg0', 'cmHg', 'inHg32', 'inHg60', 'inHg', 'ftHg', 'kgf/cm2', 'kgf/m2',
                        'kgf/mm2', 'kip/in2', 'kPa', 'lbf/ft2', 'torr']
                }
            })

            .state('conversion-rate', {
                url: '/conversion-rate',
                templateUrl: 'templates/conversion.html',
                controller: 'ConversionCtrl',
                data: {
                    title: 'Flow rate',
                    units: ['bbl/d', 'm3/d', 'MMcf/d', 'Mcf/d', 'm3/s', 'm3/h', 'm3/min', 'l/d', 'l/h', 'l/min', 'l/s', 'ft3/d', 'ft3/h', 'ft3/min',
                        'ft3/s', 'bbl/h', 'bbl/min', 'bbl/s', 'galUk/d', 'galUk/h', 'galUk/min', 'galUk/s', 'galUs/d', 'galUs/h',
                        'galUs/min', 'galUs/s']
                }
            })

            .state('conversion-volume', {
                url: '/conversion-volume',
                templateUrl: 'templates/conversion.html',
                controller: 'ConversionCtrl',
                data: {
                    title: 'Volume',
                    units: ['bbl', 'm3', 'l', 'dm3', 'cm3', 'ft3', 'in3', 'bushUs', 'galUs', 'galUk', 'gillUs', 'gillUk', 'ozUs', 'ozUk', 'stere', 'yd3', 'aft']
                }
            })

            .state('dry-gas-rate', {
                url: '/dry-gas-rate',
                templateUrl: 'templates/dry-gas-rate.html',
                controller: 'DryGasRateCtrl'
            })

            .state('plate-choice', {
                url: '/plate-choice',
                templateUrl: 'templates/plate-choice.html',
                controller: 'PlateChoiceCtrl'
            })

        ;
      
        // if none of the above states are matched, use this as the fallback
        $urlRouterProvider.otherwise('/dry-gas-rate');
        
        if (navigator.platform.match('Mac') !== null) {
            document.body.setAttribute('class', 'mac');
        }
    });

