function Measure(input) {
    var numberRegex = /(-?\d*\.?\d+)([\w\/]+)/;
    var parsed = numberRegex.exec(input);
    this.value = +parsed[1];
    this.unit = parsed[2];
    this.baseValue = Measure.converter(this.unit, true)(this.value);
}

Measure.prototype.to = function (targetUnit) {
    var targetValue = Measure.converter(targetUnit, false)(this.baseValue);
    return new Measure(targetValue + targetUnit);
};

Measure.prototype.toFixed = function (digits) {
    return parseFloat(this.value.toFixed(digits));
};

Measure.converter = function (unit, toBase) {
    var value = Measure.units[unit];
    if (typeof value.toBase === 'number') {
        if (toBase) {
            return x => x * value.toBase;
        } else {
            return x => x / value.toBase;
        }
    } else {
        return value[toBase ? 'toBase' : 'fromBase'];
    }
};

Measure.units = {
    
    // Pressure        
    'Pa': { toBase: 1, name: 'Pascal' },
    'atm': { toBase: 101325, name: 'atmosphere' },
    'at': { toBase: 98066.5, name: 'technical at.' },
    'bar': { toBase: 100000, name: 'bar' },
    'mbar': { toBase: 100, name: 'millibar' },
    'dyn/cm2': { toBase: 0.1, name: 'dyne / cm²' },
    'mmH2O': { toBase: 9.80665, name: 'mm water' },
    'cmH2O4': { toBase: 98.0638, name: 'cm water at 4°C' },
    'cmH2O': { toBase: 98.0665, name: 'cm water' },
    'inH2O39': { toBase: 249.082, name: 'in water at 39°F' },
    'inH2O60': { toBase: 248.84, name: 'in water at 60°F' },
    'inH2O': { toBase: 249.0889, name: 'in water' },
    'ftH2O39': { toBase: 2988.98, name: 'ft water at 39°F' },
    'ftH2O': { toBase: 2989.067, name: 'ft water' },
    'gf/cm2': { toBase: 98.0665, name: 'gf / cm²' },
    'mmHg': { toBase: 133.3224, name: 'mm Hg' },
    'cmHg0': { toBase: 1333.22, name: 'cm Hg at 0°C' },
    'cmHg': { toBase: 1333.224, name: 'cm Hg' },
    'inHg32': { toBase: 3386.38, name: 'in Hg at 32°F' },
    'inHg60': { toBase: 3376.85, name: 'in Hg at 60°F' },
    'inHg': { toBase: 3386.389, name: 'in Hg' },
    'ftHg': { toBase: 40636.66, name: 'ft Hg' },
    'kgf/cm2': { toBase: 98066.5, name: 'kgf / cm²' },
    'kgf/m2': { toBase: 9.80665, name: 'kgf / m²' },
    'kgf/mm2': { toBase: 9806650, name: 'kgf / mm²' },
    'kip/in2': { toBase: 6894757, name: 'kip / in²' },
    'kPa': { toBase: 1000, name: 'kilo Pascal' },
    'lbf/ft2': { toBase: 47.88026, name: 'lbf / ft²' },
    'psi': { toBase: 6894.757, name: 'psi' },
    'torr': { toBase: 133.3224, name: 'torr' },
        
    // Rate
    'm3/s': { toBase: 1, name: 'm³ / second' },
    'm3/d': { toBase: 1 / 86400, name: 'm³ / day' },
    'm3/h': { toBase: 1 / 3600, name: 'm³ / hour' },
    'm3/min': { toBase: 1 / 60, name: 'm³ / minute' },
    'l/d': { toBase: 1 / 1000 / 86400, name: 'litre / day' },
    'l/h': { toBase: 1 / 1000 / 3600, name: 'litre / hour' },
    'l/min': { toBase: 1 / 1000 / 60, name: 'litre / minute' },
    'l/s': { toBase: 1 / 1000, name: 'litre / second' },
    'MMcf/d': { toBase: 1000000 / 35.3147 / 86400, name: 'MMcf / day' },
    'Mcf/d': { toBase: 1000 / 35.3147 / 86400, name: 'Mcf / day' },
    'ft3/d': { toBase: 1 / 35.3147 / 86400, name: 'ft³ / day' },
    'ft3/h': { toBase: 1 / 35.3147 / 3600, name: 'ft³ / hour' },
    'ft3/min': { toBase: 1 / 35.3147 / 60, name: 'ft³ / minute' },
    'ft3/s': { toBase: 1 / 35.3147, name: 'ft³ / second' },
    'bbl/d': { toBase: 1 / 6.2898 / 86400, name: 'bbl / day' },
    'bbl/h': { toBase: 1 / 6.2898 / 3600, name: 'bbl / hour' },
    'bbl/min': { toBase: 1 / 6.2898 / 60, name: 'bbl / minute' },
    'bbl/s': { toBase: 1 / 6.2898, name: 'bbl / second' },
    'galUk/d': { toBase: 1 / 219.969 / 86400, name: 'UK gallons / day' },
    'galUk/h': { toBase: 1 / 219.969 / 3600, name: 'UK gallons / hour' },
    'galUk/min': { toBase: 1 / 219.969 / 60, name: 'UK gallons / minute' },
    'galUk/s': { toBase: 1 / 219.969, name: 'UK gallons / second' },
    'galUs/d': { toBase: 1 / 264.172 / 86400, name: 'US gallons / day' },
    'galUs/h': { toBase: 1 / 264.172 / 3600, name: 'US gallons / hour' },
    'galUs/min': { toBase: 1 / 264.172 / 60, name: 'US gallons / minute' },
    'galUs/s': { toBase: 1 / 264.172, name: 'US gallons / second' },
        
    // Volume
    'm3': { toBase: 1, name: 'm³' },
    'dm3': { toBase: 1 / 1000, name: 'dm³' },
    'cm3': { toBase: 1 / 1000000, name: 'cm³' },
    'l': { toBase: 1 / 1000, name: 'litre' },
    'ft3': { toBase: 1 / 35.3147, name: 'ft³' },
    'in3': { toBase: 1 / 61023.7, name: 'in³' },
    'bbl': { toBase: 1 / 6.2898, name: 'bbl' },
    'bushUs': { toBase: 1 / 28.3776, name: 'US bushel' },
    'galUs': { toBase: 1 / 264.172, name: 'US gallon' },
    'galUk': { toBase: 1 / 219.969, name: 'UK gallon' },
    'gillUs': { toBase: 1 / 8453.51, name: 'US gill' },
    'gillUk': { toBase: 1 / 7039.02, name: 'UK gill' },
    'ozUs': { toBase: 1 / 33814, name: 'US ounce' },
    'ozUk': { toBase: 1 / 35195.1, name: 'UK ounce' },
    'stere': { toBase: 1 / 1, name: 'stere' },
    'yd3': { toBase: 1 / 1.30795, name: 'yd³' },
    'aft': { toBase: 1 / 0.000810714, name: 'aft' },
    
    // Temperature
    'K': { toBase: 1, name: 'Kelvin' },
    'C': { toBase: x => x + 273.15, fromBase: x => x - 273.15, name: 'Celsius' },
    'F': { toBase: x => (x + 459.67) * 5 / 9, fromBase: x => x * 9 / 5 - 459.67, name: 'Fahrenheit' },
    'R': { toBase: 5 / 9, name: 'Rankine' }
};
    