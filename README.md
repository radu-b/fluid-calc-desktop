# README #

This is desktop application for computing a few common formulas and conversions that are used in well testing.

## Installing ##

You can download the pre-built installers for [Windows](https://bitbucket.org/radu-b/fluid-calc-desktop/downloads/FluidCalc_Setup.exe) or [Mac OS X](https://bitbucket.org/radu-b/fluid-calc-desktop/downloads/FluidCalc.dmg).

## Compiling and Running ##

To build the project, you need [Node.js](https://nodejs.org/en/download/) v4 or later.

First, get the dependencies using a command terminal open at the root of the project and run:
```
npm install
```

Then start the application:
```
npm start
```

## Building Installers ##

To build the installer for Windows (which can be done only on Windows):
```
npm run pack:win
```

To build the installer for Mac OS X (which can be done only on a Mac):
```
npm run pack:osx
```

## Screenshot ##

![screenshot](https://bitbucket.org/radu-b/fluid-calc-desktop/raw/master/assets/screenshot.png)